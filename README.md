<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

Standalone Ironic setup
=======================

This repository contains a standalone docker-compose setup for Openstack Ironic,
based on [Bifrost](https://github.com/openstack/bifrost) and a previous [manual setup](docs/manual_ironic_setup.md).

This setup consists of the ironic-api, -conductor and -inspector services, with
MariaDB as the database backend, Apache as web server and dnsmasq as DHCP and TFTP server.

Installation
------------

### Dependencies

You will need to install `docker-compose`, or [podman-compose](https://github.com/containers/podman-compose) if you are on centos 8.
Note that `podman-compose` is still in early development, and you will need to install the `devel` branch from source,
as the current `0.1.5` release has [issues with volume creation](https://github.com/containers/podman-compose/issues/61).

### Configuration

You should change the files under `config/` to fit your setup:

* `config/dnsmasq.conf`

      # disable dns for now
      port=0
      
      user=root
      
      bind-interfaces
      
      listen-address=192.0.2.1
      
      dhcp-range=192.0.2.101,192.0.2.101,255.255.255.0
      dhcp-host=00:1e:67:e4:0a:81,192.0.2.101
      
      enable-tftp
      tftp-root=/tftpboot
      
      # chainload ipxe if the node isn't running it already
      # (ipxe characteristically sends a 175 option)
      dhcp-match=set:ipxe,175
      dhcp-boot=tag:!ipxe,/undionly.kpxe
      
      # otherwise continue with the boot.ipxe script
      dhcp-boot=http://192.0.2.1:8080/boot.ipxe

  Here you will need to set the `listen-address`, `dhcp-range`, and `dhcp-host`
  for any hosts that you want to have a specific address.
  You also need to set the host for the second `dhcp-boot`, which will probably be the same as the listen-address.

* `config/ironic.conf`

      [DEFAULT]
      debug = true
      auth_strategy = noauth
      rpc_transport = json-rpc
      
      enabled_network_interfaces = noop
      enabled_inspect_interfaces = no-inspect
      enabled_power_interfaces = ipmitool
      enabled_management_interfaces = ipmitool
      
      enabled_hardware_types = ipmi
      
      [pxe]
      pxe_append_params = console=ttyS0 ipa-insecure=1
      pxe_config_template = $pybasedir/drivers/modules/ipxe_config.template
      tftp_server = 192.0.2.1
      tftp_root = /tftpboot
      tftp_master_path = /tftpboot/master_images
      tftp_master_path =
      pxe_bootfile_name = undionly.kpxe
      ipxe_enabled = true
      ipxe_boot_script = /etc/ironic/boot.ipxe
      enable_netboot_fallback = true
      
      [deploy]
      http_url = http://192.0.2.1:8080/
      http_root = /httpboot
      default_boot_option = local
      erase_devices_priority = 0
      erase_devices_metadata_priority = 10
      
      [conductor]
      api_url = http://192.0.2.1:6385/
      automated_clean = true
      deploy_kernel = http://192.0.2.1:8080/ipa.kernel
      deploy_ramdisk = http://192.0.2.1:8080/ipa.initramfs
      rescue_kernel = http://192.0.2.1:8080/ipa.kernel
      rescue_ramdisk = http://192.0.2.1:8080/ipa.initramfs
      
      [database]
      connection=mysql+pymysql://ironic:c5YlylhT5TxO@mariadb/ironic?charset=utf8
      
      [dhcp]
      dhcp_provider=none
      
      [json_rpc]
      auth_strategy = noauth
      host_ip = conductor
      port = 8089

  Again, you have to replace the IPs of the `tftp_server` and the http urls.

* `config/default.ipxe`

      #!ipxe
        
      dhcp || reboot
      
      goto introspect
      
      :introspect
      kernel http://192.0.2.1:8080/ipa.kernel ipa-inspection-callback-url=http://192.0.2.1:5050/v1/continue ipa-api-url=http://192.0.2.1:6385 systemd.journald.forward_to_console=yes BOOTIF=${mac} nofb nomodeset vga=normal console=ttyS0 ipa-insecure=1 initrd=ipa.initramfs
      initrd http://192.0.2.1:8080/ipa.initramfs
      boot

  Same here.

* If you want to change the database credentials, they are being set in `config/init.sql`.

### Firewalld

You will also need to open the ports for the dnsmasq service:

    firewall-cmd --add-service=dhcp --permanent
    firewall-cmd --add-service=tftp --permanent
    firewall-cmd --add-port=8080/tcp --permanent
    firewall-cmd --reload

This is necessary because the dnsmasq container runs in host network mode, which is required for dhcp.
For all other services Podman will autonmatically create temporary rules for each forwarded port.

This also means that you should avoid running `firewall-cmd --reload` after the other services are running,
because it will remove all temporary firewall rules.

### SELinux

SELinux should be disabled for things to work correctly.
Update `/etc/selinux/config` and set the following:

```
SELINUX=permissive
```

Reboot the system afterwards.

### Starting the Services

Make sure your current directory is the root of this repository and run

    docker-compose up -d

The `-d` (detach) will make the services start up in the background.

To stop all services run

    docker-compose down

(You can replace docker-compose with `podman-compose`, of course.)

### Building Images

Two of services, `diskimage-builder` and `client` will exit immediatly if started in detached mode.
They are utility services that are meant to be run in interactive mode.

The purpose of `diskimage-builder` is to generate both the ironic-python-agent ramdisk image and the target operating system images.
It contains contains two scripts that are mounted in from the `config/` directory.
They can be run with

    docker-compose run --rm diskimage-builder ./build_ipa.sh
    docker-compose run --rm diskimage-builder ./build_centos8.sh

The resulting images will be copied into the http volume, to be accessible via the web-server.

Both scripts use the diskimage-builders `devuser` element to add an authorized\_keys file to the ssh config of each images default `centos` user.
The included keys can be configured under `config/authorized_keys`.

You can add aditional packages to the centos image by editing `config/build_centos8.sh` and adding the `-p package[,p2...]` option.

Usage
-----

Just like `diskimage-builder`, the `client` service will just exit when run in detached mode.

However, running

    docker-compose run --rm client

will start a bash shell inside a new client service container.
It has the openstack baremetal client installed, and the environment variables set to talk to the api.

For example, new nodes can be created with

    openstack baremetal node create --driver ipmi --name testnode

and examined with

    openstack baremetal node show testnode

### Discovery

This Ironic setup is configured to support node discovery.

When nodes unknown to Ironic perform a net-boot, they will per default boot the ipa-ramdisk,
which will examine the node and provide information about the it to the ironic-inspector.
The inspector will then create a new node object and a new port object for the nodes netboot interface.

It will also fill the node object with some basic information about the node, such as Disk size and CPU capabilities.
If available, it will also set the ipmi\_address field in the driver\_info structure.

The ironic-inspector can be configured with a [rules DSL](https://docs.openstack.org/ironic-inspector/stein/user/usage.html#discovery) to
fill in further attributes based on the information received from the ramdisk.

### Basic node config

Since we do not currently have any such rules, there is a number of attributes we need to set manually before the node can be deployed:

    openstack baremetal node set \
        --name 'testnode' \
        --deploy-interface direct
        --driver-info ipmi_username=root \
        --driver-info ipmi_password=XXXXXX \
        --instance-info image_source=http://192.0.2.1:8080/centos8.qcow2 \
        --instance-info image_checksum=340165e64de723ca3bcf43beca954d26 \
        --property capabilities='boot_mode:uefi' \
        <Node UUID>

A name is not strictly required but may be more convenient than UUIDs.

The `direct` deploy interface will use the ipa image to write the image to disk from the http server.

The ipmi-credentials are required by the ipmi driver to control the powerstate of the node.

The `--instance-info` options specify the installation source, where `image_checksum` is the md5sum of centos8.qcow2.

You can leave out the `boot_mode:uefi` capability if you want to boot in Legacy mode.
        
### Node deployment

Newly created nodes will be in the `enroll` state.

    openstack baremetal node manage <Node>

will put them into the manageable `state`, and set the `maintainance` flag.

    openstack baremetal node provide <Node>

will attempt to clean a managable node, and if successful, will clear the `maintainance` flag and put it into the `available` state.
It can then be deployed with

    openstack baremetal node deploy <Node> [--config-drive <config-drive>]

`<config-drive>` can be either a JSON string or a path to configdrive data. See [here](https://docs.openstack.org/ironic/train/install/configdrive.html#when-used-standalone) for details.

A graph of the available states can be found [here](https://docs.openstack.org/ironic/train/_images/states.svg).

One useful state transmission that is missing from this graph is `openstack baremetal node undeploy`, which will clean the node after failed or successful deployment and put it in the `available` state again.

### Usage of the config drive

The following example will describe the steps needed to provision a bond interface using the config drive.

1. Create the directory tree `config-drive/openstack/latest`
1. Create the file `config-drive/openstack/latest/meta_data.json` with the content `{"uuid": "<some uuid>"}`
1. Create the file `config-drive/openstack/latest/network_data.json` with the following content (replace as appropriate)

```
{
  "links": [
      {
        "type": "phy",
        "id": "eth1",
        "name": "eth1"
      },
      {
        "type": "phy",
        "id": "eth3",
        "name": "eth3"
      },
      {
        "type": "bond",
        "id": "bond0",
        "bond_links": [
          "eth1",
          "eth3"
        ],
        "bond_mode": "802.3ad"
      }
  ],
  "networks": [
    {
      "link": "bond0",
      "type": "ipv4",
      "id": "clemens-ip",
      "ip_address": "198.51.100.101",
      "netmask": "255.255.255.0",
      "gateway": "198.51.100.1",
      "dns_nameservers": ["198.51.100.2", "198.51.100.3"]
    }
  ]
}
```

You can then deploy the server using `openstack baremetal node deploy <nodename> --config-drive <path to config-drive/>`.
So that if `/abc123/config-drive/openstack/latest/meta_data.json` is the path to the `meta_data.json` then the command is `openstack baremetal node deploy <nodename> --config-drive /abc123/config-drive/`

## License

[Apache 2](LICENSE.txt)
