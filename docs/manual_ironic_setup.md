<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

Basic standalone Ironic setup (deprecated)
==========================================

This is currently specific to the ironic-setup in C&Hs dd1001b deployment,
where all services are installed on a single controller (`192.0.2.1`), running centos 8,
and only a single node (`00:1e:67:e4:0a:81`, `192.0.2.101`) is to be provisioned.
The subnet is considered trusted, so everything just uses plain http without any authentication.

The ironic services and the python http server where just started in a screen session

MariaDB
-------

* Install MariaDB

      yum install mariadb-server
      systemctl enable mariadb
      systemctl start mariadb

* Create ironic database and user

      CREATE USER ironic IDENTIFIED BY "c5YlylhT5TxO";
      CREATE DATABASE ironic;
      GRANT ALL PRIVILEGES ON ironic.* TO ironic;
      
      CREATE USER inspector IDENTIFIED BY "FTPSGx02nA24";
      CREATE DATABASE inspector;
      GRANT ALL PRIVILEGES ON inspector.* TO inspector;
      
      FLUSH PRIVILEGES;

Ironic Conductor and API
------------------------

* Install the Train release of Ironic into a venv

      yum install gcc python3-devel python3 git
      python3 -m venv /usr/venv-ironic
      . /usr/venv-ironic/bin/activate
      git clone https://github.com/openstack/requirements.git --depth 1 --branch stable/train
      git clone https://github.com/openstack/ironic.git --depth 1 --branch stable/train
      pip3 install -c requirements/upper-constraints.txt pymysql ironic/

* Create `/etc/ironic/ironic.conf` with the following content:

      [DEFAULT]
      debug = true
      auth_strategy = noauth
      rpc_transport = json-rpc

      enabled_network_interfaces = noop
      enabled_inspect_interfaces = no-inspect
      enabled_power_interfaces = ipmitool
      enabled_management_interfaces = ipmitool

      enabled_hardware_types = ipmi,manual-management
      
      [pxe]
      pxe_append_params = console=ttyS0 ipa-insecure=1
      pxe_config_template = $pybasedir/drivers/modules/ipxe_config.template
      tftp_server = 192.0.2.1
      tftp_root = /tftpboot
      tftp_master_path = /tftpboot/master_images
      pxe_bootfile_name = undionly.kpxe
      ipxe_enabled = true
      ipxe_boot_script = /etc/ironic/boot.ipxe
      enable_netboot_fallback = true
      
      [deploy]
      http_url = http://192.0.2.1:8080/
      http_root = /httpboot
      default_boot_option = local
      erase_devices_priority = 0
      erase_devices_metadata_priority = 10
      
      [conductor]
      api_url = http://192.0.2.1:6385/
      automated_clean = true
      deploy_kernel = http://192.0.2.1:8080/ipa.kernel
      deploy_ramdisk = http://192.0.2.1:8080/ipa.initramfs
      rescue_kernel = http://192.0.2.1:8080/ipa.kernel
      rescue_ramdisk = http://192.0.2.1:8080/ipa.initramfs
      
      [database]
      # The SQLAlchemy connection string to use to connect to the
      # database. (string value)
      connection=mysql+pymysql://ironic:c5YlylhT5TxO@localhost/ironic?charset=utf8
      
      [dhcp]
      dhcp_provider=none
      
      [json_rpc]
      auth_strategy = noauth
      host_ip = 127.0.0.1
      port = 8089

* The `/etc/ironic/boot.ipxe` script template is taken from Bifrost:

      #!ipxe
      
      isset ${mac:hexhyp} && goto boot_system ||
      chain ipxe.pxe
      
      # load the MAC-specific file or fail if it's not found
      :boot_system
      chain {{ ipxe_for_mac_uri }}${mac:hexhyp} || goto inspector_ipa
      
      :inspector_ipa
      chain {{ ipxe_for_mac_uri }}default || goto error_no_config
      
      :error_no_config
      echo PXE boot failed. No configuration found for MAC ${mac}
      echo Press any key to reboot...
      prompt --timeout 180 || reboot
      reboot

* The API has to be reachable from node later on, so we need to open the port:

      firewall-cmd --zone=public --add-port=6385/tcp

* The conductor needs a few extra packages

      yum install qemu-img ipmitool

* Run `ironic-dbsync` to create the tables in the database.

* The services can be started with the `ironic-api` and `ironic-conductor` commands.

Ironic Inspector
----------------

* I installed the inspector into the same venv as ironic itself:

      . /usr/venv-ironic/bin/activate
      git clone https://github.com/openstack/ironic-inspector.git --depth 1 --branch stable/train
      pip3 install -c requirements/upper-constraints.txt ironic-inspector/

* `/etc/ironic-inspector/ironic-inspector.conf`:

      [DEFAULT]
      auth_strategy = noauth
      debug = true
      transport_url = fake://
      
      [database]
      connection=mysql+pymysql://inspector:FTPSGx02nA24@localhost/inspector?charset=utf8
      
      [pxe_filter]
      driver = noop
      
      [ironic]
      auth_type = none
      endpoint_override = http://192.0.2.1:6385/
      
      [processing]
      add_ports = pxe
      keep_ports = present
      always_store_ramdisk_logs = true
      store_data = database
      node_not_found_hook = enroll
      power_off = false
      
      [discovery]
      enroll_node_driver = ipmi
      # enroll_node_driver = manual-management

* Run `ironic-inspector-dbsync upgrade` to create the tables in the database.

* The services can be started with the `ironic-inspector` command.

DHCP
----

* Standalone ironic needs a separate dhcp server. Bifrost uses dnsmasq, I used dhcpd for this.

      yum install dhcp-server

* `/etc/dhcp/dhcpd.conf`:

      subnet 192.0.2.0 netmask 255.255.255.0 {
      }
      
      host m1r1 {
        hardware ethernet 00:1e:67:e4:0a:81;
        fixed-address 192.0.2.101;
        if exists user-class and option user-class = "iPXE" {
              filename "http://192.0.2.1:8080/boot.ipxe";
        } else {
              filename "/undionly.kpxe";
              next-server 192.0.2.1;
        }
      }

  If the request comes from an iPXE bootloader it will return a url of the `boot.ipxe` script, wich contains further boot instructions.

  Otherwise the `undionly` iPXE bootloader is provided via tftp.

* The server is not enabled by default

      systemctl enable dhcpd
      systemctl start dhcpd

TFTP
----

The tftp-server package comes with systemd unit file. Bifrost disables this unit and starts the service via xinetd.
I'm not sure what the benefits are, but I copied it.

* Install tftp and requirements

      yum install tftp-server syslinux-tftpboot xinetd ipxe-bootimgs

* This config should be created as `/etc/xinetd.d/tftp`:

      service tftp
      {
        bind            = 192.0.2.1
        protocol        = udp
        port            = 69
        socket_type     = dgram
        wait            = yes
        # Note(TheJulia): While the line below looks incorrect, tftp-hpa changes its
        # effective user by default to the nobody user.
        user            = root
        server          = /usr/sbin/in.tftpd
        server_args     = -v -v -v -v -s --map-file /tftpboot/map-file /tftpboot
        disable         = no
        flags           = IPv4
      }

* `/tftpboot/map-file` should have the following contents:

      r ^([^/]) /tftpboot/\1
      r ^(/tftpboot/) /tftpboot/\2

* The iPXE bootloader must be placed in the tftp dirctory

      cp /usr/share/ipxe/undionly.kpxe /tftpboot/

* Files in `/tftpboot` need the right SELinux context, also we need to open the port:

      chcon -R system_u:object_r:tftpdir_t:s0 /tftpboot
      firewall-cmd --zone=public --add-port=69/udp

* Disable tftp.service and restart xinetd

      systemctl disable tftp
      systemctl stop tftp
      systemctl restart xinetd

HTTP
----

* Bifrost uses nginx, I just used the python builtin http server for quick and dirty testing
      
      mkdir /httpboot
      cd /httpboot
      firewall-cmd --zone=public --add-port=8080/tcp
      python3 -m http.server 8080

* The ironic conductor will create the 'boot.ipxe' script, but we need to provide the file `/httpboot/pxelinux.cfg/default` with the following content:

      #!ipxe
      
      dhcp || reboot
      
      goto introspect
      
      :introspect
      kernel http://192.0.2.1:8080/ipa.kernel ipa-inspection-callback-url=http://192.0.2.1:5050/v1/continue ipa-api-url=http://192.0.2.1:6385 systemd.journald.forward_to_console=yes BOOTIF=${mac} nofb nomodeset vga=normal console=ttyS0 ipa-insecure=1 initrd=ipa.initramfs
      initrd http://192.0.2.1:8080/ipa.initramfs
      boot

* we also need to provide the `ipa.kernel` and `ipa.initramfs`. I'm using the upstream train image for now:
      
      wget https://tarballs.opendev.org/openstack/ironic-python-agent/dib/files/ipa-centos7-stable-train.kernel -O /httpboot/ipa.kernel
      wget https://tarballs.opendev.org/openstack/ironic-python-agent/dib/files/ipa-centos7-stable-train.initramfs -O /httpboot/ipa.initramfs

Baremetal Client
----------------

* I installed this one in the same venv as the ironic services:

      . /usr/venv-ironic/bin/activate
      pip3 install -c requirements/upper-constraints.txt python-openstackclient python-ironicclient

* To use the client with a standalone ironic service, you need to export the following environment variables:

      export OS_AUTH_TYPE=none
      export OS_ENDPOINT=http://192.0.2.1:6385/
