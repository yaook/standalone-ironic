#!/bin/sh
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -eux

export ELEMENTS_PATH=/usr/local/share/ironic-python-agent-builder/dib

export DIB_RELEASE=8

# These keep diskimage-builder from using python2 for the image
# (causing build to fail)
export DIB_REPOREF_requirements=origin/stable/train
export DIB_REPOREF_ironic_python_agent=origin/stable/train

export DIB_DEV_USER_USERNAME=centos
export DIB_DEV_USER_PWDLESS_SUDO=yes
export DIB_DEV_USER_AUTHORIZED_KEYS=./authorized_keys

mdadm_path="$ELEMENTS_PATH/mdadm-kill"
mdadm_script="$mdadm_path/post-install.d/10-mdadm-kill"
mkdir "$mdadm_path"
touch "$mdadm_path/README.rst"
touch "$mdadm_path/element-deps"
mkdir "$mdadm_path/post-install.d"
printf '#!/bin/bash\necho "DEVICE /dev/null" > /etc/mdadm.conf\n' > "$mdadm_script"
chmod +x "$mdadm_script"

disk-image-create -o ipa ironic-python-agent-ramdisk devuser openssh-server centos-minimal mdadm-kill

mv ipa.initramfs ipa.kernel /httpboot/
