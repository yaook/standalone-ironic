##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
# disable dns for now
port=0

user=root

bind-interfaces

listen-address=192.0.2.1

dhcp-range=192.0.2.101,192.0.2.101,255.255.255.0
dhcp-host=00:1e:67:e4:0a:81,192.0.2.101

enable-tftp
tftp-root=/tftpboot

# Filter if we are booting using ipxe
dhcp-match=set:ipxe,175

# Filter if we are booting an uefi system
dhcp-match=set:efi,option:client-arch,7
dhcp-match=set:efi,option:client-arch,9
dhcp-match=set:efi,option:client-arch,11

# Boot the ipxe image if we use ipxe
dhcp-boot=tag:ipxe,http://192.0.2.1:8080/boot.ipxe

# Boot the ipxe chainloader for efi
# Note that we need to filter for !ipxe as we overwrite it otherwise
dhcp-boot=tag:efi,tag:!ipxe,/ipxe.efi

# Boot the ipxe chainloader for bios
# We do not need to filter for !ipxe because the last entry behaves differently
dhcp-boot=/undionly.kpxe

